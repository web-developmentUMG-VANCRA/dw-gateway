package com.vancra.proyecto_pogra;
import org.springframework.data.jpa.repository.JpaRepository;
import java.io.Serializable;
import java.util.List;

public interface telefonosRepository extends JpaRepository<telefonos,Serializable>{
	List <telefonos> deleteByNumero(int numero);
}
