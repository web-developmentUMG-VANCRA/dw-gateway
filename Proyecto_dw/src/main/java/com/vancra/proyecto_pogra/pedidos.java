package com.vancra.proyecto_pogra;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table
public class pedidos implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private int idpedidos;

	@Column
	private String nombre;
	@Column
	private double montototal;
	@Column
	private int vendedoracodigo;
	@Column
	private LocalDate fechae;
	

	@OneToMany(mappedBy = "pedidoid", cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	private List<productospedidos> productospedidos;
	
	public LocalDate getFechae() {
		return fechae;
	}
	public void setFechae(LocalDate fechae) {
		this.fechae = fechae;
	}
	
	public int getVendedoracodigo() {
		return vendedoracodigo;
	}
	public void setVendedoracodigo(int vendedoracodigo) {
		this.vendedoracodigo = vendedoracodigo;
	}
	
	public int getIdpedido() {
		return idpedidos;
	}
	public void setIdpedido(int idpedido) {
		this.idpedidos = idpedido;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public double getDescripcion() {
		return montototal;
	}
	public void setDescripcion(double montototal) {
		this.montototal = montototal;
	}
	
	public List<productospedidos> getproductospedidos() {
        return productospedidos;
    }
	public void setProductospedidos(List<productospedidos> productospedidos) {
		this.productospedidos = productospedidos;
	}
}
