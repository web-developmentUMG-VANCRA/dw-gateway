package com.vancra.proyecto_pogra;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table
public class productos implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private int idproductos;
	@Column
	private double costounitario;
	@Column
	private String nombre;
	@Column
	private String descripcion;
	@Column
	private int cantidad;
	@Column
	private String imagen;
	
	@OneToMany(mappedBy = "productoid", cascade = CascadeType.ALL)
	private Set<productospedidos> productospedidos;
	
	public int getIdproductos() {
		return idproductos;
	}
	public void setIdproductos(int idproductos) {
		this.idproductos = idproductos;
	}
	public double getCostounitario() {
		return costounitario;
	}
	public void setCostounitario(double costounitario) {
		this.costounitario = costounitario;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public void setCantidad(int cantidad){
		this.cantidad=cantidad;
	}
	public int getCantidad(){
		return cantidad;
	}
	public String getImagen() {
		return imagen;
	}
	public void setImagen(String imagen) {
		this.imagen = imagen;
	}
}
