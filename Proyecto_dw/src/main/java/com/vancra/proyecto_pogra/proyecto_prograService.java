package com.vancra.proyecto_pogra;

import java.util.List;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.EmitterProcessor;
import reactor.core.publisher.Flux;

@RestController


public class proyecto_prograService {
	@Autowired
	vendedorasRepository vendedorasRepository;
	@Autowired
	telefonosRepository telefonosRepository;
	
	@RequestMapping(value="/vendedoras/all",method=RequestMethod.GET)
	public List<vendedoras> findAll(){
		return vendedorasRepository.findAll();
	}

	@RequestMapping(value="/vendedoras/save",method=RequestMethod.POST)
	public vendedoras saveVendedora(@RequestBody vendedoras vendedora) {
		
		List<telefonos> telefonos = vendedora.getTelefono();
		vendedora.setTelefono(null);
		vendedoras temp = vendedorasRepository.save(vendedora);
		for(telefonos t: telefonos) {
			t.setCodigovendedora(temp.getCodigo());
			telefonosRepository.save(t);
		}
		temp.setTelefono(telefonos);
		return temp;
	}

	@DeleteMapping("/vendedoras/{codigo}")
	public void deleteVendedora(@PathVariable int codigo) {
		vendedorasRepository.deleteById(codigo);
	}

	@Transactional
	@DeleteMapping("/telefonos/{idtelefonos}")
	public void deleteNumero(@PathVariable int idtelefonos) {
		telefonosRepository.deleteById(idtelefonos);
	}

	@PutMapping("/vendedoras/{codigo}")
	public ResponseEntity<Object> updateVendedora(@RequestBody vendedoras vendedora, @PathVariable int codigo) {
		List<telefonos> telefonos = vendedora.getTelefono();
		vendedora.setTelefono(null);
		vendedora.setCodigo(codigo);
		vendedoras temp = vendedorasRepository.save(vendedora);
		for(telefonos t: telefonos) {
			t.setCodigovendedora(temp.getCodigo());
			telefonosRepository.save(t);
		}
		temp.setTelefono(telefonos);
		return ResponseEntity.noContent().build();
	}

	@GetMapping("/vendedoras/by/nombre/{nombre}")
	public List <vendedoras> findByNombre(@PathVariable("nombre")String nombre){
		return vendedorasRepository.findByNombre(nombre);
	}

	@GetMapping(value="/vendedoras/find/id/{codigo}")
	public vendedoras findById(@PathVariable("codigo") int codigo) {
		return vendedorasRepository.findById(codigo).get();
	}
	@GetMapping("/telefonos/all")
	public List <telefonos> findAllt(){
		return telefonosRepository.findAll();
	}
	
	@Autowired
	anunciosRepository anunciosRepository;
	@GetMapping(value="/anuncios/all")
	public List<anuncios> findAllA(){
		return anunciosRepository.findAll();
	}
	
	@PostMapping(value="/anuncios/save")
	public anuncios saveItem(@RequestBody anuncios anuncios) {
		return anunciosRepository.save(anuncios);
	}

	@GetMapping(value="/anuncios/find/id/{codigo}")
	public anuncios findByIda(@PathVariable("codigo") int codigo) {
		return anunciosRepository.findById(codigo).get();
	}
	@DeleteMapping("/anuncios/{idanuncios}")
	public void deleteAnuncio(@PathVariable int idanuncios) {
		anunciosRepository.deleteById(idanuncios);
	}
	@PutMapping("/anuncios/{idanuncios}")
	public ResponseEntity<Object> updateAnuncio(@RequestBody anuncios anuncio, @PathVariable int idanuncios) {
		anuncio.setIdanuncios(idanuncios);
		anunciosRepository.save(anuncio);
		return ResponseEntity.noContent().build();
	}
	
	@Autowired
	productosRepository productosRepository;

	@GetMapping(value="/productos/all")
	public List<productos> findAllPro(){
		return productosRepository.findAll();
	}
	
	@PostMapping(value="/productos/save")
	public productos saveItem(@RequestBody productos productos){
		productosRepository.save(productos);
		notificationProcessor.onNext(productos);
		return productos;
	}

	@GetMapping(value="/productos/find/id/{idproductos}")
	public productos findByIdp(@PathVariable("idproductos") int idproductos) {
		return productosRepository.findById(idproductos).get();
	}

	@DeleteMapping("/productos/{idproductos}")
	public void deleteProducto(@PathVariable int idproductos) {
		productos p=productosRepository.findById(idproductos).get();
		productosRepository.deleteById(idproductos);
		notificationProcessor.onNext(p);
	}

	@PutMapping("/productos/{idproductos}")
	public ResponseEntity<Object> updateProducto(@RequestBody productos producto, @PathVariable int idproductos) {
		producto.setIdproductos(idproductos);
		productosRepository.save(producto);
		notificationProcessor.onNext(producto);
		return ResponseEntity.noContent().build();
	}
	
	
	@Autowired
	productospedidosRepository productospedidosRepository;
	
	
	@GetMapping(value="/productospedidos/all")
	public List<productospedidos> findAllProp(){
		return productospedidosRepository.findAll();
	}
	
	@PostMapping(value="/productospedidos/save")
	public productospedidos saveItem(@RequestBody productospedidos productospedidos){
		return productospedidosRepository.save(productospedidos);
	}
	
	
	@Autowired
	pedidosRepository pedidosRepository;
	@GetMapping(value="/pedidos/all")
	public List<pedidos> findAllPe(){
		return pedidosRepository.findAll();
	}
	
	@PostMapping(value="/pedidos/save")
	public pedidos saveItem(@RequestBody pedidos pedido){
		
		List<productospedidos> productospedidos = pedido.getproductospedidos();
		pedido.setProductospedidos(null);
		pedidos temp = pedidosRepository.save(pedido);
		for(productospedidos p: productospedidos) {
			p.setPedidoid(temp.getIdpedido());
			productospedidosRepository.save(p);
		}
		temp.setProductospedidos(productospedidos);
		notificationProcessor2.onNext(temp);
		return temp;
	}
	
	@GetMapping("/pedidos/by/ven/{vendedoracodigo}")
	public List <pedidos> findByVendedora(@PathVariable("vendedoracodigo")int vendedoracodigo){
		return pedidosRepository.findByVendedoracodigo(vendedoracodigo);
	}

	private EmitterProcessor<productos> notificationProcessor;
	private EmitterProcessor<pedidos> notificationProcessor2;

    /* -------------------------------------------------------------------------------------------------------------- */
    @PostConstruct
    private void createProcessor() {
		notificationProcessor = EmitterProcessor.create();
		notificationProcessor2= EmitterProcessor.create();
    }
	private Flux<ServerSentEvent<productos>> getProductoSSE() {
        return notificationProcessor
                .log().map(
                        (p) -> {
                            System.out.println("Sending Producto:" + p.getCantidad());
							return ServerSentEvent.<productos>builder()
								.id(UUID.randomUUID().toString())
								.event("Product-update")
                                .data(p)
                                .build();
                        }).concatWith(Flux.never());
	}

	private Flux<ServerSentEvent<pedidos>> getPedidosSSE() {
        return notificationProcessor2
                .log().map(
                        (pe) -> {
                            System.out.println("Sending Pedido:" + pe.getIdpedido());
							return ServerSentEvent.<pedidos>builder()
								.id(UUID.randomUUID().toString())
								.event("Pedido-update")
                                .data(pe)
                                .build();
                        }).concatWith(Flux.never());
	}

	@GetMapping(value = "/notification/sse")
    public Flux<ServerSentEvent<?>>
            getJobResultNotification() {
				return Flux.merge(
					getProductoSSE(),
					getPedidosSSE()
				);
			}
}
